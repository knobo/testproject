(in-package :cl-user)
(defpackage testproject-test
  (:use :cl
        :testproject
        :prove))
(in-package :testproject-test)

;; NOTE: To run this test file, execute `(asdf:test-system :testproject)' in your Lisp.

(plan nil)

;; blah blah blah.

(finalize)
