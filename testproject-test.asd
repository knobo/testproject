#|
  This file is a part of testproject project.
|#

(in-package :cl-user)
(defpackage testproject-test-asd
  (:use :cl :asdf))
(in-package :testproject-test-asd)

(defsystem testproject-test
  :author ""
  :license ""
  :depends-on (:testproject
               :prove)
  :components ((:module "t"
                :components
                ((:test-file "testproject"))))
  :description "Test system for testproject"

  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run-test-system) :prove-asdf) c)
                    (asdf:clear-system c)))
